
Corelations with lag
====================



sj with 0.20 threshold
---------------------

| Feature | lag | correlation |
|---------|-----|-------------|
|reanalysis_air_temp_k                    |8  | 0.2978951033010247  |
|reanalysis_avg_temp_k                    |8  | 0.2949370392683012  |
|reanalysis_dew_point_temp_k              |8  | 0.30202059484302995 |
|reanalysis_max_air_temp_k                |7  | 0.29540004018873134 |
|reanalysis_min_air_temp_k                |8  | 0.3033024748029085  |
|reanalysis_specific_humidity_g_per_kg    |8  | 0.30166424005058784 |
|station_avg_temp_c                       |10 | 0.3689968112853393  |
|station_max_temp_c                       |11 | 0.315344116644517   |
|station_min_temp_c                       |10 | 0.36238757342797945 |


iq with 0.10 threshold
---------------------
| Feature | lag | correlation |
|---------|-----|-------------|
|ndvi_ne                         | 11  |0.13949708299410885 |
|ndvi_nw                         | 11  |0.14499934757033708 |
|ndvi_sw                         | 11  |0.1589962217002258  |
|precipitation_amt_mm            | 3   |0.12507203980695986 |
|reanalysis_air_temp_k           | 7   |0.14843050332961552 |
|reanalysis_avg_temp_k           | 9   |0.14823478566921816 |
|reanalysis_max_air_temp_k       | 10  |0.14370319741353788 |
|reanalysis_sat_precip_amt_mm    | 3   |0.12507203980695986 |
|station_avg_temp_c              | 5   |0.14233340213432633 |
|station_diur_temp_rng_c         | 10  |0.12980736837712722 |
|station_max_temp_c              | 6   |0.1586400011900955  |
|station_precip_mm               | 3   |0.1250649840650182  |


correlated feature pairs
------------------------

sj
--

* reanalysis_avg_temp_k - reanalysis_air_temp_k
* reanalysis_min_air_temp_k - reanalysis_air_temp_k
* reanalysis_min_air_temp_k - reanalysis_avg_temp_k
* reanalysis_dew_point_temp_k - reanalysis_relative_humidity_percent

iq
--
* reanalysis_dew_point_temp_k - reanalysis_specific_humidity_g_per_kg


Observations
------------

* removing ndvi features gives best results
* When a lag gets higher it becomes less relevant even when thers a higher correlation
* TODO: try removing 'reanalysis_max_air_temp_k' from sj model


