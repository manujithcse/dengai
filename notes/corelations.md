Corelations without lag
=======================

sj
---

| Feature | pcc |
|---------|-----|
|**reanalysis_specific_humidity_g_per_kg**    |**0.207947**|
|**reanalysis_dew_point_temp_k**              |**0.203774**|
|**station_avg_temp_c**                       |**0.196617**|
|**reanalysis_max_air_temp_k**                |**0.194532**|
|station_max_temp_c                       |0.189901|
|reanalysis_min_air_temp_k                |0.187943|
|reanalysis_air_temp_k                    |0.181917|
|station_min_temp_c                       |0.177012|
|reanalysis_avg_temp_k                    |0.175267|
|reanalysis_relative_humidity_percent     |0.144045|
|reanalysis_precip_amt_kg_per_m2          |0.107457|
|ndvi_nw                                  |0.075307|
|precipitation_amt_mm                     |0.060211|
|reanalysis_sat_precip_amt_mm             |0.060211|
|station_precip_mm                        |0.051759|
|ndvi_ne                                  |0.037639|
|station_diur_temp_rng_c                  |0.034630|
|ndvi_se                                  |0.001113|
|ndvi_sw                                 |-0.000333|
|reanalysis_tdtr_k                       |-0.067600|

sj lagged 12
------------

| Feature | pcc |
|---------|-----|
|station_avg_temp_c_lagged|                      0.349938|
|station_min_temp_c_lagged|                      0.337475|
|station_max_temp_c_lagged|                      0.300285|
|reanalysis_min_air_temp_k_lagged|               0.260385|
|reanalysis_dew_point_temp_k_lagged|             0.258475|
|reanalysis_specific_humidity_g_per_kg_lagged|   0.255310|
|reanalysis_avg_temp_k_lagged|                   0.247391|
|reanalysis_air_temp_k_lagged|                   0.246900|
|reanalysis_max_air_temp_k_lagged|               0.243547|
|reanalysis_specific_humidity_g_per_kg|          0.207947|
|reanalysis_dew_point_temp_k|                    0.203774|
|station_avg_temp_c|                             0.196617|
|reanalysis_max_air_temp_k|                      0.194532|
|station_max_temp_c|                             0.189901|
|reanalysis_min_air_temp_k|                      0.187943|
|reanalysis_air_temp_k|                          0.181917|
|station_min_temp_c|                             0.177012|
|reanalysis_avg_temp_k|                          0.175267|
|reanalysis_relative_humidity_percent_lagged|    0.148913|
|reanalysis_relative_humidity_percent|           0.144045|
|ndvi_nw_lagged|                                 0.108274|
|reanalysis_precip_amt_kg_per_m2|                0.107457|
|ndvi_nw|                                        0.075307|
|reanalysis_sat_precip_amt_mm|                   0.060211|
|precipitation_amt_mm|                           0.060211|
|station_precip_mm|                              0.051759|
|ndvi_ne_lagged|                                 0.049949|
|reanalysis_precip_amt_kg_per_m2_lagged|         0.037920|
|reanalysis_sat_precip_amt_mm_lagged|            0.037644|
|precipitation_amt_mm_lagged|                    0.037644|
|ndvi_ne|                                        0.037639|
|station_diur_temp_rng_c|                        0.034630|
|station_diur_temp_rng_c_lagged|                 0.013598|
|ndvi_se|                                        0.001113|
|ndvi_sw|                                       -0.000333|
|station_precip_mm_lagged|                      -0.013772|
|ndvi_sw_lagged|                                -0.018464|
|ndvi_se_lagged|                                -0.055147|
|reanalysis_tdtr_k_lagged|                      -0.060008|
|reanalysis_tdtr_k|                             -0.067600|

iq
---

| Feature | pcc |
|---------|-----|
|reanalysis_specific_humidity_g_per_kg    |0.236476|
|reanalysis_dew_point_temp_k              |0.230401|
|reanalysis_min_air_temp_k                |0.214514|
|station_min_temp_c                       |0.211702|
|reanalysis_relative_humidity_percent     |0.130083|
|station_avg_temp_c                       |0.113070|
|reanalysis_precip_amt_kg_per_m2          |0.101171|
|reanalysis_air_temp_k                    |0.097098|
|precipitation_amt_mm                     |0.090171|
|reanalysis_sat_precip_amt_mm             |0.090171|
|reanalysis_avg_temp_k                    |0.079872|
|station_max_temp_c                       |0.075279|
|station_precip_mm                        |0.042976|
|ndvi_sw                                  |0.032999|
|ndvi_ne                                  |0.020215|
|ndvi_nw                                 |-0.009586|
|ndvi_se                                 |-0.041067|
|reanalysis_max_air_temp_k               |-0.056474|
|station_diur_temp_rng_c                 |-0.058230|
|reanalysis_tdtr_k                       |-0.134425|
