from base_model import *
import pprint
import pandas as pd
import numpy as np

import statsmodels.api as sm
from statsmodels.tools import eval_measures
import statsmodels.formula.api as smf

from matplotlib import pyplot as plt
import seaborn as sns
import utils
import preprocessor
import feature_extractor

from base_model import get_nb_model


def get_nb_model_formula(features):
        model_formula = "total_cases ~ 1"
        for k, v in features.items():
                model_formula = model_formula + " + " + k
        return model_formula


def get_lagged_nb_model(train, test, features):

    # Step 1: specify the form of the model
    model_formula = get_nb_model_formula(features)

    grid = 10 ** np.arange(-8, -3, dtype=np.float64)

    best_alpha = []
    best_score = 1000

    # Step 2: Find the best hyper parameter, alpha
    for alpha in grid:
        model = smf.glm(formula=model_formula,
                        data=train,
                        family=sm.families.NegativeBinomial(alpha=alpha))

        results = model.fit()
        predictions = results.predict(test).astype(int)
        score = eval_measures.meanabs(predictions, test.total_cases)

        if score < best_score:
            best_alpha = alpha
            best_score = score

    print('best alpha = ', best_alpha)
    print('best score = ', best_score)

    # Step 3: refit on entire dataset
    full_dataset = pd.concat([train, test])
    model = smf.glm(formula=model_formula,
                    data=full_dataset,
                    family=sm.families.NegativeBinomial(alpha=best_alpha))

    fitted_model = model.fit()
    return fitted_model

def feature_selection(dataframe, city):
    if city == "sj":
        return dataframe[['reanalysis_specific_humidity_g_per_kg', 
                 'reanalysis_dew_point_temp_k', 
                 'station_avg_temp_c', 
                 'station_min_temp_c',
                 'ndvi_se',
                 'ndvi_nw',
                 'reanalysis_precip_amt_kg_per_m2',
                 'reanalysis_relative_humidity_percent',
                 'total_cases',
                 ]]
    elif city == "iq":
        return dataframe[['reanalysis_specific_humidity_g_per_kg', 
                 'reanalysis_dew_point_temp_k', 
                 'reanalysis_min_air_temp_k', 
                 'station_min_temp_c',
                 'total_cases',
                 'ndvi_nw',
                 'ndvi_ne',
                 'ndvi_sw',
                 'station_precip_mm']]


def run_nb_model():

        sj_data, iq_data = utils.read_train_data()
        sj_all, iq_all = preprocessor.get_test_train_integrated_data()


        # sj_all = feature_selection(sj_all, "sj")
        # iq_all = feature_selection(iq_all, "iq")

        # sj_all = preprocessor.fill_missing_values(sj_all)
        # iq_all = preprocessor.fill_missing_values(iq_all)

        preprocessor.remove_non_climatic_features(sj_all,iq_all)

        sj_all_lagged, sj_lags \
                = feature_extractor.get_lagged_feature_df(sj_all, "sj")
        iq_all_lagged, iq_lags \
                = feature_extractor.get_lagged_feature_df(iq_all, "iq")

        # seperate training and predict data
        sj_data_lagged = sj_all_lagged.head(936)
        sj_predict_lagged = sj_all_lagged.tail(sj_all_lagged.shape[0] - 936)
        iq_data_lagged = iq_all_lagged.head(520)
        iq_predict_lagged = iq_all_lagged.tail(iq_all_lagged.shape[0] - 520)

        # sj_data_lagged = preprocessor.smooth_features(sj_data_lagged)
        # iq_data_lagged = preprocessor.smooth_features(iq_data_lagged)
        
        # split test and train data
        sj_train_lagged, sj_test_lagged \
                = utils.split_train_test_in_order(sj_data_lagged)
        iq_train_lagged, iq_test_lagged \
                = utils.split_train_test_in_order(iq_data_lagged)
        
        sj_train_lagged.dropna(inplace=True)
        iq_train_lagged.dropna(inplace=True)
        sj_test_lagged.dropna(inplace=True)
        iq_test_lagged.dropna(inplace=True)
        sj_predict_lagged.fillna(method="ffill", inplace=True)
        iq_predict_lagged.fillna(method="ffill", inplace=True)

        # fitting train features into a common scale
        sj_train_lagged = preprocessor.scale_features(sj_train_lagged)
        sj_test_lagged = preprocessor.scale_features(sj_test_lagged)
        iq_train_lagged = preprocessor.scale_features(iq_train_lagged)
        iq_test_lagged = preprocessor.scale_features(iq_test_lagged)

        sj_predict_lagged = preprocessor.scale_features(sj_predict_lagged)
        iq_predict_lagged = preprocessor.scale_features(iq_predict_lagged)

        sj_model = get_lagged_nb_model(sj_train_lagged, sj_test_lagged,
                                       sj_lags)
        iq_model = get_lagged_nb_model(iq_train_lagged, iq_test_lagged,
                                       iq_lags)


        figs, axes = plt.subplots(nrows=2, ncols=1)

        sj_data['fitted'] = sj_model.fittedvalues
        sj_data.fitted.plot(ax=axes[0], label="Predictions")
        sj_data.total_cases.plot(ax=axes[0], label="Actual")

        iq_data['fitted'] = iq_model.fittedvalues
        iq_data.fitted.plot(ax=axes[1], label="Predictions")
        iq_data.total_cases.plot(ax=axes[1], label="Actual")

        
        
        sj_predicts = sj_model.predict(sj_predict_lagged).astype(int)
        iq_predicts = iq_model.predict(iq_predict_lagged).astype(int)

        save_results(sj_predicts, iq_predicts, "sj_lagged_predicts")

        plt.legend(loc='upper right', fancybox=True)
        plt.show()


if __name__ == "__main__":

        '''
        TODO:
        combine/remove highly correlated features
        find a legit way to smooth this shit
        imputation for missing values
        check sklearn feature selection modules
        check on invidual distributions of features
        '''

        run_nb_model()