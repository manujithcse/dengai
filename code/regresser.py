import pandas as pd
import numpy as np
from keras.models import Sequential
from keras.layers import Dense
from keras.wrappers.scikit_learn import KerasRegressor
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import KFold
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import Pipeline

# define base model
def baseline_model():
    model = Sequential()
    model.add(Dense(12, input_dim=21, kernel_initializer='normal', activation='relu'))
    model.add(Dense(4, kernel_initializer='normal', activation='relu'))
    # model.add(Dense(2, kernel_initializer='normal', activation='relu'))
    model.add(Dense(1, kernel_initializer='normal'))
    model.compile(loss='mae', optimizer='adam')
    return model

def get_test_set():
    dataframe = pd.read_csv("../dataset/DengAI_Predicting_Disease_Spread_-_Test_Data_Features.csv")

    dataframe.replace('', np.nan, inplace=True)
    dataframe.fillna(method='ffill', inplace=True)
    # dataframe.dropna(inplace=True)
    dataframe.reset_index(inplace=True, drop=True)

    dataframe = dataframe.loc[dataframe['city'] == 'iq']

    X = dataframe.iloc[:,[0,2,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23]]
    X_1 = X.iloc[:, 1:]

    return X_1

# load dataset
dataframe = pd.read_csv("../dataset/DengAI_Predicting_Disease_Spread_-combined.csv")

dataframe.replace('', np.nan, inplace=True)
dataframe.dropna(inplace=True)
dataframe.reset_index(inplace=True, drop=True)

dataframe = dataframe.loc[dataframe['city'] == 'iq']

X = dataframe.iloc[:,[0,2,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23]]
Y = dataframe.iloc[:, 24]
X_1 = X.iloc[:, 1:]

X_test = get_test_set()

# print X_test

seed = 7
np.random.seed(seed)
# estimator = KerasRegressor(build_fn=baseline_model, epochs=100, batch_size=50, verbose=1)

# kfold = KFold(n_splits=2, random_state=seed)
# results = cross_val_score(estimator, X_1.values, Y.values, cv=kfold)
# print("Results: %.2f (%.2f) MSE" % (results.mean(), results.std()))

estimators = []
estimators.append(('standardize', StandardScaler()))
estimators.append(('mlp', KerasRegressor(build_fn=baseline_model, epochs=10000, batch_size=10, verbose=1, validation_split=0.2)))
pipeline = Pipeline(estimators)

# kfold = KFold(n_splits=4, random_state=seed)
# results = cross_val_score(pipeline, X_1.values, Y.values, cv=kfold)

print ""
pipeline.fit(X_1.values, Y.values)
y_1 = pipeline.predict(X_test.values)
# print y_1


# estimator.fit(X_1.values, Y.values)
# y_1 = estimator.predict(X_1.values)
# y_1 = estimator.predict(X_test.values)

# print X_1.iloc[:,[0,2]]
# print y_1
# print y_1.tolist()


for v in y_1.tolist():
    if v == float('NaN'):
        print 0
    else:
        print int(round(v))


# Y_test = estimator.predict(X_test.values)
# print Y_test



