import utils
import preprocessor
import matplotlib.pyplot as plt
import seaborn as sns
import sklearn
from sklearn.model_selection import KFold, cross_val_score
import pandas as pd
from keras.wrappers.scikit_learn import KerasRegressor
from keras.models import Sequential
from keras.layers import Dense, Activation, LSTM
from sklearn.pipeline import Pipeline
from sklearn.decomposition import PCA
from sklearn.feature_selection import SelectKBest
from sklearn.pipeline import FeatureUnion

sns.set(color_codes=True)

def distribs(dataframe):
    fig, axs = plt.subplots(ncols=5, nrows=4)

    k = 0
    for i in range(4):
        for j in range(5):
            sns.distplot(dataframe.iloc[:, k], ax=axs[i][j])
            k += 1

    plt.show()

def regressor_model():
    model = Sequential()
    model.add(Dense(50, input_dim=20,
                         kernel_initializer='random_uniform', 
                         bias_initializer='random_uniform', 
                         activation='relu'))

    # model.add(Dense(10, kernel_initializer='random_uniform', 
    #                      bias_initializer='random_uniform', 
    #                      activation='relu'))

    model.add(Dense(1, kernel_initializer='random_uniform'))
    model.compile(loss='mae', optimizer='adam')
    # model.compile()
    return model

sj_data, iq_data = utils.read_train_data()
sj_test, iq_test = utils.read_test_data()


# preprocessor.remove_non_climatic_features(sj_data, iq_data)
# preprocessor.remove_non_climatic_features(sj_test, iq_test)

sj_data = preprocessor.fill_missing_values(sj_data, "ffill")
sj_test = preprocessor.fill_missing_values(sj_test, "ffill")

iq_data = preprocessor.fill_missing_values(iq_data, "ffill")
iq_test = preprocessor.fill_missing_values(iq_test, "ffill")

y_sj = sj_data["total_cases"]
x_sj = sj_data.drop("total_cases", axis=1)
sj_test = sj_test.drop("total_cases", axis=1)

y_iq = iq_data["total_cases"]
x_iq = iq_data.drop("total_cases", axis=1)
iq_test = iq_test.drop("total_cases", axis=1)

seed = 7

features = []
# features.append(('pca', PCA(n_components=10)))
# features.append(('select_best', SelectKBest(k=10)))
# feature_union = FeatureUnion(features)

estimators = []
# estimators.append(('feature_union', feature_union))
estimators.append(('standardize', sklearn.preprocessing.StandardScaler()))
estimators.append(("mlp", KerasRegressor(build_fn=rnn_model, 
                           epochs=100, 
                           batch_size=50, 
                           verbose=1)))
kfold = KFold(n_splits=10, random_state=seed)

sj_model = Pipeline(estimators)
iq_model = Pipeline(estimators)


results = cross_val_score(sj_model, x_sj, y_sj, cv=kfold)
print "\n MEAN SJ"
print(results.mean())

results = cross_val_score(iq_model, x_iq, y_iq, cv=kfold)
print "\n MEAN IQ"
print(results.mean())

figs, axes = plt.subplots(nrows=2, ncols=1)

result_all = sj_model.fit(x_sj, y_sj)
sj_data['fitted'] = sj_model.predict(x_sj).astype(int)
sj_data.fitted.plot(label="Predictions", ax=axes[0])
sj_data.total_cases.plot(label="Actual", ax=axes[0])

result_all = iq_model.fit(x_iq, y_iq)
iq_data['fitted'] = iq_model.predict(x_iq).astype(int)
iq_data.fitted.plot(label="Predictions", ax=axes[1])
iq_data.total_cases.plot(label="Actual", ax=axes[1])

sj_predicts = sj_model.predict(sj_test).astype(int)
iq_predicts = iq_model.predict(iq_test).astype(int)

print "\nMAE"
print sklearn.metrics.mean_absolute_error(sj_data.total_cases.tolist(), sj_data.fitted.tolist())
print sklearn.metrics.mean_absolute_error(iq_data.total_cases.tolist(), iq_data.fitted.tolist())

utils.save_results(sj_predicts, iq_predicts, "dnn_1")

plt.legend(loc='upper right', fancybox=True)
plt.show()
# print x_sj
# sj_data_norm = pd.DataFrame(sj_data_norm)
# print sj_data_norm

# distribs(pd.DataFrame(x_sj))
