
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
import seaborn as sns


def read_train_data():
    train_data = pd.read_csv("../dataset/train_combined.csv",
                             index_col=[0, 1, 2])
    train_data.drop(['week_start_date'], axis=1, inplace=True)
    return (train_data.loc["sj"], train_data.loc["iq"])


def read_test_data():
    train_data = pd.read_csv("../dataset/test_features.csv",
                             index_col=[0, 1, 2])
    train_data.drop(['week_start_date'], axis=1, inplace=True)
    train_data["total_cases"] = 0
    return (train_data.loc["sj"], train_data.loc["iq"])


def visualize_heatmap(dataframe):
    corelations = dataframe.corr()
    sns.heatmap(corelations)
    plt.show()


def visualize_corelations(dataframe):
    corelations = dataframe.corr()
    corelations.total_cases.drop('total_cases')
    corelations.total_cases.sort_values(ascending=False).plot.barh()
    plt.show()


def save_results(sj_predicts, iq_predicts, name):
    submission = pd.read_csv("../dataset/submission_format.csv",
                             index_col=[0, 1, 2])
    submission.total_cases = np.concatenate([sj_predicts, iq_predicts])
    submission.to_csv("../results/%s.csv" % name)


def split_train_test_random(df):
    train = df.sample(frac=0.8, random_state=200)
    test = df.drop(train.index)
    return (train, test)


def split_train_test_in_order(df):
    train = df.head(int(df.shape[0]*0.85))
    test = df[train.shape[0]:]
    return (train, test)


def plot_fitted(dataframe):
    figs, axes = plt.subplots(nrows=1, ncols=1)
    dataframe.fitted.plot(label="Predictions")
    dataframe.total_cases.plot(label="Actual")
    plt.show()


def visualize_lagged_correlations(dataframe, lag):
    dataframe.total_cases = dataframe.total_cases.shift(-lag)
    dataframe_processed = dataframe.dropna()
    visualize_corelations(dataframe_processed)


def visualize_lagged_heatmap(dataframe, lag):
    dataframe.total_cases = dataframe.total_cases.shift(-lag)
    dataframe_processed = dataframe.dropna()
    visualize_heatmap(dataframe_processed)
