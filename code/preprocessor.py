
'''
all preprocessing related functionality is implemented here
    -data integration
    -filling missing values
    -smoothing
    -standardization of data
    -normalization of data
    -data transformations
'''

import utils
from sklearn import preprocessing
import pandas as pd


def get_test_train_integrated_data():
    sj_data, iq_data = utils.read_train_data()
    sj_test, iq_test = utils.read_test_data()
    sj_all = sj_data.append(sj_test)
    iq_all = iq_data.append(iq_test)
    return (sj_all, iq_all)


def fill_missing_values(df, type="linear"):

    if(type == "ffill"):
        df.fillna(method="ffill", inplace=True)
        return df
    elif(type == "linear"):
        df.interpolate(method='linear', inplace=True)
        return df
    elif(type == "mean_impute"):
        total_cases = df[['total_cases']]
        total_cases.reset_index(inplace=True)
        index_cols = total_cases[['weekofyear', 'year']] 
        df_temp = df.drop(['total_cases'], axis=1)
        headers = df_temp.dtypes.index
        x = df_temp.values
        imputer = preprocessing.Imputer(strategy='mean')
        imputed_climatic_features = imputer.fit_transform(x)
        result = pd.DataFrame(imputed_climatic_features)
        result.columns = headers
        result['total_cases'] = total_cases['total_cases']
        result['year'] = index_cols['year']
        result['weekofyear'] = index_cols['weekofyear']
        result.reset_index(inplace=True)
        result.drop(['index'], axis=1, inplace=True)
        result.set_index(['year', 'weekofyear'], inplace=True)
        return result


def scale_features(df):
    total_cases = df[['total_cases']]
    total_cases.reset_index(inplace=True)
    index_cols = total_cases[['weekofyear', 'year']] 
    df_temp = df.drop(['total_cases'], axis=1)
    headers = df_temp.dtypes.index
    x = df_temp.values  # returns a numpy array
    scaler = preprocessing.StandardScaler().fit(x)
    x_scaled = scaler.transform(x)
    result = pd.DataFrame(x_scaled)
    result.columns = headers
    result['total_cases'] = total_cases['total_cases']
    result['year'] = index_cols['year']
    result['weekofyear'] = index_cols['weekofyear']
    result.reset_index(inplace=True)
    result.drop(['index'], axis=1, inplace=True)
    result.set_index(['year', 'weekofyear'], inplace=True)
    return result


def remove_non_climatic_features(sj, iq):
    sj.drop(['week_start_date'], axis=1, inplace=True)
    iq.drop(['week_start_date'], axis=1, inplace=True)


def smooth_features(dataframe, window=24):
    masked_headers = ['total_cases']
    mask = ~(dataframe.columns.isin(masked_headers))
    cols_to_smooth = dataframe.columns[mask]
    dataframe[cols_to_smooth] = pd.rolling_mean(
                                dataframe.loc[:, mask],
                                window,
                                center=True)
    return dataframe
