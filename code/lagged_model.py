import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
import base_model as bm
import statsmodels.api as sm
from statsmodels.tools import eval_measures
import statsmodels.formula.api as smf

def feature_selection(dataframe, city):
    if city == "sj":
        return dataframe[['station_avg_temp_c', 
                 'station_min_temp_c', 
                 'station_max_temp_c', 
                 'reanalysis_min_air_temp_k',
                 'total_cases']]
    elif city == "iq":
        return dataframe[['reanalysis_specific_humidity_g_per_kg', 
                 'reanalysis_dew_point_temp_k', 
                 'reanalysis_min_air_temp_k', 
                 'station_min_temp_c',
                 'total_cases']]

def get_nb_model(train, test, city):
    # Step 1: specify the form of the model
    if city == "sj":
        model_formula = "total_cases ~ 1 + " \
                        "station_avg_temp_c + " \
                        "station_min_temp_c + " \
                        "station_max_temp_c + " \
                        "reanalysis_min_air_temp_k"
    elif city == "iq":
        model_formula = "total_cases ~ 1 + " \
                        "reanalysis_specific_humidity_g_per_kg + " \
                        "reanalysis_dew_point_temp_k + " \
                        "reanalysis_min_air_temp_k + " \
                        "station_min_temp_c"
    
    grid = 10 ** np.arange(-8, -3, dtype=np.float64)
                    
    best_alpha = []
    best_score = 1000
        
    # Step 2: Find the best hyper parameter, alpha
    for alpha in grid:
        model = smf.glm(formula=model_formula,
                        data=train,
                        family=sm.families.NegativeBinomial(alpha=alpha))

        results = model.fit()
        predictions = results.predict(test).astype(int)
        score = eval_measures.meanabs(predictions, test.total_cases)

        if score < best_score:
            best_alpha = alpha
            best_score = score

    print('best alpha = ', best_alpha)
    print('best score = ', best_score)
            
    # Step 3: refit on entire dataset
    full_dataset = pd.concat([train, test])
    model = smf.glm(formula=model_formula,
                    data=full_dataset,
                    family=sm.families.NegativeBinomial(alpha=best_alpha))

    fitted_model = model.fit()
    return fitted_model

def run_lagged_model(lag):
    sj_data, iq_data = bm.read_train_data()

    sj_data_lagged = sj_data.shift(lag)
    sj_data_lagged.dropna(how="all", inplace=True)
    sj_data_lagged["total_cases"] = sj_data["total_cases"]
    sj_data_lagged.dropna(thresh=1, inplace=True)

    sj_data = feature_selection(sj_data_lagged, "sj")
    iq_data = bm.feature_selection(iq_data, "iq")

    sj_data = bm.pre_process(sj_data)
    iq_data = bm.pre_process(iq_data)

    print sj_data

    sj_train, sj_test = sj_data.head(800), sj_data.tail(sj_data.shape[0] - 800)
    iq_train, iq_test = iq_data.head(500), iq_data.tail(iq_data.shape[0] - 500)

    sj_model = get_nb_model(sj_train, sj_test, "sj")
    iq_model = bm.get_nb_model(iq_train, iq_test, "iq")

    figs, axes = plt.subplots(nrows=2, ncols=1)

    sj_data['fitted'] = sj_model.fittedvalues
    sj_data.fitted.plot(ax=axes[0], label="Predictions")
    sj_data.total_cases.plot(ax=axes[0], label="Actual")

    iq_data['fitted'] = iq_model.fittedvalues
    iq_data.fitted.plot(ax=axes[1], label="Predictions")
    iq_data.total_cases.plot(ax=axes[1], label="Actual")

    test_sj, test_iq = bm.read_test_data()
    test_sj = bm.pre_process(bm.feature_selection(test_sj, "sj"))
    test_iq = bm.pre_process(bm.feature_selection(test_iq, "iq"))

    # sj_predicts = sj_model.predict(test_sj).astype(int)
    # iq_predicts = iq_model.predict(test_iq).astype(int)

    # bm.save_results(sj_predicts, iq_predicts, "base_model_2-lagged-12")

    plt.show()

if __name__ == "__main__":
    run_lagged_model(12)