import pandas as pd
import numpy as np

from matplotlib import pyplot as plt
import seaborn as sns

import statsmodels.api as sm
from statsmodels.tools import eval_measures
import statsmodels.formula.api as smf

from sklearn.tree import DecisionTreeRegressor, ExtraTreeRegressor
from sklearn.ensemble import RandomForestRegressor, AdaBoostRegressor, GradientBoostingRegressor, ExtraTreesRegressor, BaggingRegressor
import sklearn
from sklearn.pipeline import Pipeline

from sklearn.decomposition import PCA
from sklearn.grid_search import GridSearchCV
from sklearn.linear_model import SGDRegressor

from sklearn.svm import SVR
from mlxtend.regressor import StackingRegressor
from sklearn.linear_model import LinearRegression

def get_nb_model_formula(features):
        model_formula = "total_cases ~ 1"
        for k in features:
                model_formula = model_formula + " + " + k
        return model_formula


def get_lagged_nb_model(train, test, features):

    # Step 1: specify the form of the model
    model_formula = get_nb_model_formula(features)

    grid = 10 ** np.arange(-8, -3, dtype=np.float64)

    best_alpha = []
    best_score = 1000

    # Step 2: Find the best hyper parameter, alpha
    for alpha in grid:
        model = smf.glm(formula=model_formula,
                        data=train,
                        family=sm.families.NegativeBinomial(alpha=alpha))

        results = model.fit()
        predictions = results.predict(test).astype(int)
        score = eval_measures.meanabs(predictions, test.total_cases)

        if score < best_score:
            best_alpha = alpha
            best_score = score

    print('best alpha = ', best_alpha)
    print('best score = ', best_score)

    # Step 3: refit on entire dataset
    full_dataset = pd.concat([train, test])
    model = smf.glm(formula=model_formula,
                    data=full_dataset,
                    family=sm.families.NegativeBinomial(alpha=best_alpha))

    fitted_model = model.fit()
    return fitted_model

all_data = pd.read_csv("../dataset/sj/selected.csv", index_col=[0, 1, 2])

all_data.fillna(method="ffill", inplace=True)

data = all_data[:926]
predict = all_data[926:]

split = int(data.shape[0]*0.80)
# print split
train = data[:split]
test = data[split:]


train_y = train["total_cases"]
train_x = train.drop("total_cases", axis=1)

test_y = test["total_cases"]
test_x = test.drop("total_cases", axis=1)

columns = list(data.columns) 
columns.remove("total_cases")

# model = get_lagged_nb_model(train, test, columns)

seed = 7

scaler = sklearn.preprocessing.StandardScaler()
train_x = scaler.fit_transform(train_x)
test_x = scaler.transform(test_x)
predict = scaler.transform(predict.drop("total_cases", axis=1))

model1 = AdaBoostRegressor(DecisionTreeRegressor(max_depth=20,
                                                max_features=5),
                                                n_estimators=500,
                                                loss="exponential", 
                                                random_state=seed)

model2 = AdaBoostRegressor(ExtraTreeRegressor(max_depth=20,
                                              max_features=5),
                                              n_estimators=500,
                                              loss="exponential", 
                                              random_state=seed)

model3 = BaggingRegressor(DecisionTreeRegressor(max_depth=10,
                                                max_features=5),
                                                n_estimators=500,
                                                random_state=seed)

model4 = RandomForestRegressor(n_estimators=500, 
                                                      max_depth=5,
                                                      max_features=20,
                                                      oob_score=True, 
                                                      random_state=seed)

model5 = SVR(kernel="rbf")
                            
svr_rbf = LinearRegression()
# svr_rbf = SVR(kernel="linear")
# svr_rbf = RandomForestRegressor(criterion="mae", max_depth=8, max_features=1)
model = StackingRegressor(regressors=[model1], 
                           meta_regressor=svr_rbf, verbose=1)

model.fit(train_x, train_y)

test_predict = model.predict(test_x).astype(int)
# # print test_predict
# # print test_y
sns.distplot(test_predict)
plt.show()
score = eval_measures.meanabs(test_predict, test_y)
print score

predicts = model.predict(predict).astype(int)
np.savetxt("stackmodel.csv", predicts, delimiter="\n", fmt="%d")