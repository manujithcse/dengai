import utils
import preprocessor
import matplotlib.pyplot as plt
import seaborn as sns
import sklearn
from sklearn.model_selection import KFold, cross_val_score
import pandas as pd
from keras.wrappers.scikit_learn import KerasRegressor
from keras.models import Sequential
from keras.layers import Dense, Activation, LSTM
from sklearn.pipeline import Pipeline
from sklearn.decomposition import PCA
from sklearn.feature_selection import SelectKBest
from sklearn.pipeline import FeatureUnion
from numpy import array, append
import numpy as np
from tqdm import tqdm

sns.set(color_codes=True)


# sj_all, iq_all = utils.read_train_data()
sj_all, iq_all = preprocessor.get_test_train_integrated_data()

sj_all = preprocessor.fill_missing_values(sj_all, "ffill")

# sj_all = sj_all[['reanalysis_specific_humidity_g_per_kg', 
#                  'reanalysis_dew_point_temp_k', 
#                  'station_avg_temp_c', 
#                  'reanalysis_max_air_temp_k',
#                  'total_cases']]

sj_x = sj_all.drop("total_cases", axis=1).values
sj_y = sj_all.iloc[:,-1].values

# print sj_all

sj_x = sklearn.preprocessing.scale(sj_x)
# sj_y = sklearn.preprocessing.scale(sj_y)

sj_y = array([[y] for y in sj_y])

# print sj_y.shape

# sj_all = sj_all.reshape(1, sj_all.shape[0], sj_all.shape[1])
sj_x = sj_x.reshape(1, sj_x.shape[0], sj_x.shape[1])
sj_y = sj_y.reshape(1, sj_y.shape[0], 1)

x = list()
y = list()

samples = list()
length = 52
for i in range(0,sj_x.shape[1]-length):
    x_sample = sj_x[:,i:i+length]
    y_sample = sj_y[:,i:i+length]

    x.append(x_sample)
    y.append(y_sample)

x = array([s for s in x])
x = x.reshape(x.shape[0], x.shape[2], x.shape[3])
y = array([s for s in y])
y = y.reshape(y.shape[0], y.shape[2], y.shape[3])

print x.shape
print y.shape
# print len(x), x[0].shape
# print len(y), y[0].shape

# print y
features = 20
# features = 4

model = Sequential()
model.add(LSTM(32, input_shape=(length, features), return_sequences=True))
model.add(Dense(1,kernel_initializer='random_uniform', 
                         bias_initializer='random_uniform', ))
model.compile(loss="mean_absolute_error", optimizer="adam")  

# for e in range(10):
#     print "EPOCH", e
#     for i in tqdm(range(836)):
#         # print "set", i
model.fit(x, y, epochs=10, verbose=1, batch_size=x.shape[0])

predicted = list()
# for i in range(len(x)):
#     predict = model.predict(x[i])
#     predicted.append(predict[:,-1,:][0][0])
    # print predict[:,-1,:][0][0]

# print len(predicted)

with open("lstm_2.csv", "w") as f:
    for v in predicted:
        f.write("%d\n" % int(v))

df = pd.DataFrame(predicted)
df["actual"] = sj_all["total_cases"].values[length:]


df.plot()
plt.show()

