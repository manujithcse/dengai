'''
Deriving new features/ Changing features
pca
combining correlated features
'''

import utils
import pprint


def get_lagged_features(dataframe, headers, lag):
        headers_all = dataframe.dtypes.index
        masked_headers = [x for x in headers_all if x not in headers]
        mask = ~(dataframe.columns.isin(masked_headers))
        cols_to_shift = dataframe.columns[mask]
        dataframe[cols_to_shift] = dataframe.loc[:, mask].shift(lag)
        return dataframe


def get_lagged_correlations(dataframe, lag=4):
        dataframe.total_cases = dataframe.total_cases.shift(-lag)
        corelations = dataframe.corr().total_cases.drop('total_cases')
        dataframe_processed = corelations.dropna()
        return dataframe_processed


def get_best_feature_lags(dataframe, city, max_lag):
        
        headers = dataframe.drop(columns=['total_cases']).dtypes.index
        lags = {}
        corelations = dataframe.corr().total_cases.drop("total_cases")
        for header in headers:
                lags[header] = [0, corelations[header]]
        for i in range(0, max_lag):
                sj_data, iq_data = utils.read_train_data()
                if(city=="sj"):
                        lagged_corr = get_lagged_correlations(sj_data, i)
                elif(city=="iq"):
                        lagged_corr = get_lagged_correlations(iq_data, i)

                for header in headers:
                        if(abs(corelations[header]) < abs(lagged_corr[header])):
                                corelations[header] = lagged_corr[header]
                                lags[header] = [i, lagged_corr[header]]    
     
        return lags


def filter_lags_by_correlation(lagged_correlations, threshold=0.2):
        for k, v in lagged_correlations.items():
                if(v[1] < threshold):
                        del lagged_correlations[k]
        return lagged_correlations


def shift_all_features(df, feature_lags):
        for k, v in feature_lags.items():
                df = get_lagged_features(df, [k], v[0])
                # print df.head(10)
        keys = feature_lags.keys() + ["total_cases"]
        df = df[keys]
        return df


def get_lagged_feature_df(df, city):
        # print df.describe()
        lags = get_best_feature_lags(df, city, 20)
        
        if city == "sj":
                filtered_corr = filter_lags_by_correlation(lags, 0.3)
        else:
                filtered_corr = filter_lags_by_correlation(lags, 0.2)
        lagged = shift_all_features(df, filtered_corr)

        return (lagged, filtered_corr)
