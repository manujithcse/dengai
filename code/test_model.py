import utils
import preprocessor
import feature_extractor
from lagged_correlation_model import get_lagged_nb_model
from utils import save_results

from matplotlib import pyplot as plt

def run_model(lag):
    sj_data, iq_data = utils.read_train_data()
    sj_all, iq_all = preprocessor.get_test_train_integrated_data()

    # preprocessor.remove_non_climatic_features(sj_all, iq_all)

    sj_all_lagged, sj_lags = feature_extractor.get_lagged_feature_df(sj_all, "sj")
    iq_all_lagged, iq_lags = feature_extractor.get_lagged_feature_df(iq_all, "iq")

    # print sj_all_lagged.head(20)
    # print sj_lags
    # print iq_lags
    # print sj_all_lagged.head(20)
    
    sj_data_lagged = sj_all_lagged.head(936)
    sj_predict_lagged = sj_all_lagged.tail(sj_all_lagged.shape[0] - 936)
    sj_data_lagged = sj_data_lagged[11:]

    iq_data_lagged = iq_all_lagged.head(520)
    iq_predict_lagged = iq_all_lagged.tail(iq_all_lagged.shape[0] - 520)

    # sj_data_lagged.dropna(inplace=True)
    # iq_data_lagged.dropna(inplace=True)
    sj_data_lagged = preprocessor.fill_missing_values(sj_data_lagged, "ffill")
    iq_data_lagged = preprocessor.fill_missing_values(iq_data_lagged, "ffill")

    sj_predict_lagged.fillna(method="ffill", inplace=True)
    iq_predict_lagged.fillna(method="ffill", inplace=True)

    sj_train_lagged, sj_test_lagged = utils.split_train_test_in_order(sj_data_lagged)
    iq_train_lagged, iq_test_lagged = utils.split_train_test_in_order(iq_data_lagged)

    sj_train_lagged = preprocessor.scale_features(sj_train_lagged)
    sj_test_lagged = preprocessor.scale_features(sj_test_lagged)
    sj_predict_lagged = preprocessor.scale_features(sj_predict_lagged)

    print sj_train_lagged["total_cases"]

    iq_train_lagged = preprocessor.scale_features(iq_train_lagged)
    iq_test_lagged = preprocessor.scale_features(iq_test_lagged)
    iq_predict_lagged = preprocessor.scale_features(iq_predict_lagged)

    sj_model = get_lagged_nb_model(sj_train_lagged, sj_test_lagged, sj_lags)
    iq_model = get_lagged_nb_model(iq_train_lagged, iq_test_lagged, iq_lags)

    figs, axes = plt.subplots(nrows=2, ncols=1)

    sj_data['fitted'] = sj_model.fittedvalues
    sj_data.fitted.plot(ax=axes[0], label="Predictions")
    sj_data.total_cases.plot(ax=axes[0], label="Actual")

    iq_data['fitted'] = iq_model.fittedvalues
    iq_data.fitted.plot(ax=axes[1], label="Predictions")
    iq_data.total_cases.plot(ax=axes[1], label="Actual")

    sj_predicts = sj_model.predict(sj_predict_lagged).astype(int)
    iq_predicts = iq_model.predict(iq_predict_lagged).astype(int)

    save_results(sj_predicts, iq_predicts, "all_predicts_7-17")

    plt.legend(loc='upper right', fancybox=True)
    plt.show()

if __name__ == "__main__":
    run_model(12)