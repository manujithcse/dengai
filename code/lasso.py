import utils
import preprocessor
import matplotlib.pyplot as plt
import seaborn as sns
import sklearn
from sklearn.model_selection import KFold, cross_val_score
import pandas as pd
from keras.wrappers.scikit_learn import KerasRegressor
from keras.models import Sequential
from keras.layers import Dense, Activation, LSTM
from sklearn.pipeline import Pipeline
from sklearn.decomposition import PCA
from sklearn.feature_selection import SelectKBest
from sklearn.pipeline import FeatureUnion
from sklearn import linear_model
from statsmodels.tools import eval_measures

sns.set(color_codes=True)


sj_data, iq_data = utils.read_train_data()
sj_test, iq_test = utils.read_test_data()

sj_data = sj_data[['reanalysis_specific_humidity_g_per_kg', 
                 'reanalysis_dew_point_temp_k', 
                 'station_avg_temp_c', 
                 'reanalysis_max_air_temp_k',
                 'total_cases']]
sj_test = sj_test[['reanalysis_specific_humidity_g_per_kg', 
                 'reanalysis_dew_point_temp_k', 
                 'station_avg_temp_c', 
                 'reanalysis_max_air_temp_k',
                 'total_cases']]

sj_data = preprocessor.fill_missing_values(sj_data, "ffill")
sj_test = preprocessor.fill_missing_values(sj_test, "ffill")

iq_data = preprocessor.fill_missing_values(iq_data, "ffill")
iq_test = preprocessor.fill_missing_values(iq_test, "ffill")

y_sj = sj_data["total_cases"]
x_sj = sj_data.drop("total_cases", axis=1)
sj_test = sj_test.drop("total_cases", axis=1)

y_iq = iq_data["total_cases"]
x_iq = iq_data.drop("total_cases", axis=1)
iq_test = iq_test.drop("total_cases", axis=1)

reg = linear_model.LassoCV(cv=10, max_iter=10000, normalize=True)
result = reg.fit(x_sj, y_sj)
print result.alpha_

sj_train_data, sj_train_test = utils.split_train_test_in_order(sj_data)
sj_train_data_y = sj_train_data["total_cases"]
sj_train_data_x = sj_train_data.drop("total_cases", axis=1)
sj_train_test_y = sj_train_test["total_cases"]
sj_train_test_x = sj_train_test.drop("total_cases", axis=1)

reg_new = linear_model.Lasso(alpha=result.alpha_)
reg_new.fit(sj_train_data_x, sj_train_data_y)
predicts = reg_new.predict(sj_train_test_x)

score = eval_measures.meanabs(predicts, sj_train_test_y)
print score

# seed = 7

# features = []
# # features.append(('pca', PCA(n_components=10)))
# # features.append(('select_best', SelectKBest(k=10)))
# # feature_union = FeatureUnion(features)

# estimators = []
# estimators.append(('feature_union', feature_union))
# estimators.append(('standardize', sklearn.preprocessing.StandardScaler()))
# estimators.append(("regress", linear_model.Lasso(alpha=0.1)))
# kfold = KFold(n_splits=10, random_state=7)

# sj_model = Pipeline(estimators)
# iq_model = Pipeline(estimators)


# results = cross_val_score(sj_model, x_sj, y_sj, cv=kfold, verbose=1, ite)
# print "\n MEAN SJ"
# print(results.mean())

# results = cross_val_score(iq_model, x_iq, y_iq, cv=kfold)
# print "\n MEAN IQ"
# print(results.mean())

# figs, axes = plt.subplots(nrows=2, ncols=1)

# result_all = sj_model.fit(x_sj, y_sj)
# sj_data['fitted'] = sj_model.predict(x_sj).astype(int)
# sj_data.fitted.plot(label="Predictions", ax=axes[0])
# sj_data.total_cases.plot(label="Actual", ax=axes[0])

# result_all = iq_model.fit(x_iq, y_iq)
# iq_data['fitted'] = iq_model.predict(x_iq).astype(int)
# iq_data.fitted.plot(label="Predictions", ax=axes[1])
# iq_data.total_cases.plot(label="Actual", ax=axes[1])

# sj_predicts = sj_model.predict(sj_test).astype(int)
# iq_predicts = iq_model.predict(iq_test).astype(int)

# print "\nMAE"
# print sklearn.metrics.mean_absolute_error(sj_data.total_cases.tolist(), sj_data.fitted.tolist())
# print sklearn.metrics.mean_absolute_error(iq_data.total_cases.tolist(), iq_data.fitted.tolist())

# utils.save_results(sj_predicts, iq_predicts, "dnn_1")

# plt.legend(loc='upper right', fancybox=True)
# plt.show()