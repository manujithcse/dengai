import pandas as pd
import numpy as np

from matplotlib import pyplot as plt
import seaborn as sns

import statsmodels.api as sm
from statsmodels.tools import eval_measures
import statsmodels.formula.api as smf

from sklearn.tree import DecisionTreeRegressor, ExtraTreeRegressor
from sklearn.ensemble import RandomForestRegressor, AdaBoostRegressor, GradientBoostingRegressor, ExtraTreesRegressor, BaggingRegressor
import sklearn
from sklearn.pipeline import Pipeline

from sklearn.decomposition import PCA
from sklearn.feature_selection import SelectKBest
from sklearn.grid_search import GridSearchCV
from sklearn.linear_model import SGDRegressor, Ridge, ElasticNet

from sklearn.svm import SVR

from keras.wrappers.scikit_learn import KerasRegressor
from keras.models import Sequential
from keras.layers import Dense, Activation, LSTM, Dropout
from keras.optimizers import Adam, SGD

np.random.seed(7)

def regressor_model():
    model = Sequential()
    model.add(Dense(128, input_dim=44, activation='relu'))
    model.add(Dense(256, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(512, activation='relu'))
    model.add(Dense(256, activation='relu'))
    model.add(Dropout(0.2))
    model.add(Dense(128, activation='relu'))
    model.add(Dense(64, activation='relu'))
    model.add(Dense(1, activation="linear"))

    optimizer = Adam(lr=0.001)
    optimizer = SGD(lr=0.01, momentum=0.8, decay=0.0, nesterov=False)
    # model.compile(loss='mae', optimizer='adam')
    model.compile(loss='mae', optimizer=optimizer)
    # model.compile()
    return model


all_data = pd.read_csv("../dataset/sj/selected.csv", index_col=[0, 1, 2])

all_data.fillna(method="ffill", inplace=True)

data = all_data[:926]
predict = all_data[926:]

split = int(data.shape[0]*0.80)
# print split
train = data[:split]
test = data[split:]

train_y = train["total_cases"]
train_x = train.drop("total_cases", axis=1)

test_y = test["total_cases"]
test_x = test.drop("total_cases", axis=1)

# print len(train_x.columns)

# estimators = []
# estimators.append(('standardize', sklearn.preprocessing.StandardScaler()))
# estimators.append(("mlp", KerasRegressor(build_fn=regressor_model, 
#                            epochs=100, 
#                            batch_size=50, 
#                            verbose=1)))

# model = Pipeline(estimators)
# model.fit(train_x, train_y)

scaler = sklearn.preprocessing.StandardScaler()
train_x = scaler.fit_transform(train_x)
test_x = scaler.transform(test_x)
predict = scaler.transform(predict)

model = regressor_model()
model.fit(train_x, train_y, batch_size=30, epochs=1,
            validation_data=[test_x, test_y])

test_predict = model.predict(test_x).astype(int)

fig, axes = plt.subplots(nrows=2)

sns.distplot(test_predict, ax=axes[0])
sns.distplot(test_y, ax=axes[0])

axes[1].plot(test_predict)
axes[1].plot(test_y.values)

plt.show()
score = eval_measures.meanabs(test_predict.flatten(), test_y)


print score
# print test_predict.flatten()

predicts = model.predict(predict.drop("total_cases", axis=1)).astype(int).flatten()
np.savetxt("dnnmodel_sj.csv", predicts, delimiter="\n", fmt="%d")