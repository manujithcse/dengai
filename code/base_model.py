import pandas as pd
import numpy as np

from matplotlib import pyplot as plt
import seaborn as sns

import statsmodels.api as sm
from statsmodels.tools import eval_measures
import statsmodels.formula.api as smf


def read_train_data():
    train_data = pd.read_csv("../dataset/train_combined.csv", index_col=[0, 1, 2])
    train_data.drop(['week_start_date'], axis=1, inplace=True)
    
    return (train_data.loc["sj"], train_data.loc["iq"])

def read_test_data():
    train_data = pd.read_csv("../dataset/test_features.csv", index_col=[0, 1, 2])
    train_data["total_cases"] = 0
    
    return (train_data.loc["sj"], train_data.loc["iq"])

def feature_selection(dataframe, city):
    if city == "sj":
        return dataframe[['reanalysis_specific_humidity_g_per_kg', 
                 'reanalysis_dew_point_temp_k', 
                 'station_avg_temp_c', 
                 'reanalysis_max_air_temp_k',
                 'total_cases']]
    elif city == "iq":
        return dataframe[['reanalysis_specific_humidity_g_per_kg', 
                 'reanalysis_dew_point_temp_k', 
                 'reanalysis_min_air_temp_k', 
                 'station_min_temp_c',
                 'total_cases']]

def pre_process(dataframe):
    dataframe.fillna(method="ffill", inplace=True)
    return dataframe

def visualize_heatmap(dataframe):
    corelations = dataframe.corr()
    sns.heatmap(corelations)
    plt.show()

def visualize_corelations(dataframe):
    corelations = dataframe.corr()
    print corelations.total_cases.drop('total_cases').sort_values(ascending=False)
    corelations.total_cases.sort_values(ascending=False).plot.barh()
    plt.show()

def get_nb_model(train, test, city):
    # Step 1: specify the form of the model
    if city == "sj":
        model_formula = "total_cases ~ 1 + " \
                        "reanalysis_specific_humidity_g_per_kg + " \
                        "reanalysis_dew_point_temp_k + " \
                        "reanalysis_max_air_temp_k + " \
                        "station_avg_temp_c"
    elif city == "iq":
        model_formula = "total_cases ~ 1 + " \
                        "reanalysis_specific_humidity_g_per_kg + " \
                        "reanalysis_dew_point_temp_k + " \
                        "reanalysis_min_air_temp_k + " \
                        "station_min_temp_c"
    
    grid = 10 ** np.arange(-8, -3, dtype=np.float64)
    print grid
    best_alpha = []
    best_score = 1000
        
    # Step 2: Find the best hyper parameter, alpha
    for alpha in grid:
        model = smf.glm(formula=model_formula,
                        data=train,
                        family=sm.families.NegativeBinomial(alpha=alpha))

        results = model.fit()
        predictions = results.predict(test).astype(int)
        score = eval_measures.meanabs(predictions, test.total_cases)

        if score < best_score:
            best_alpha = alpha
            best_score = score

    print('best alpha = ', best_alpha)
    print('best score = ', best_score)
            
    # Step 3: refit on entire dataset
    full_dataset = pd.concat([train, test])
    model = smf.glm(formula=model_formula,
                    data=full_dataset,
                    family=sm.families.NegativeBinomial(alpha=best_alpha))

    fitted_model = model.fit()
    return fitted_model

def save_results(sj_predicts, iq_predicts, name):
    submission = pd.read_csv("../dataset/submission_format.csv", index_col=[0, 1, 2])
    submission.total_cases = np.concatenate([sj_predicts, iq_predicts])
    submission.to_csv("../results/%s.csv" % name)

def plot_fitted(dataframe):
    figs, axes = plt.subplots(nrows=1, ncols=1)
    dataframe.fitted.plot(label="Predictions")
    dataframe.total_cases.plot(label="Actual")
    plt.show()

def run_base_model():
    sj_data, iq_data = read_train_data()

    sj_data = feature_selection(sj_data, "sj")
    iq_data = feature_selection(iq_data, "iq")

    sj_data = pre_process(sj_data)
    iq_data = pre_process(iq_data)

    sj_train, sj_test = sj_data.head(800), sj_data.tail(sj_data.shape[0] - 800)
    iq_train, iq_test = iq_data.head(500), iq_data.tail(iq_data.shape[0] - 500)

    sj_model = get_nb_model(sj_train, sj_test, "sj")
    iq_model = get_nb_model(iq_train, iq_test, "iq")

    figs, axes = plt.subplots(nrows=2, ncols=1)

    sj_data['fitted'] = sj_model.fittedvalues
    sj_data.fitted.plot(ax=axes[0], label="Predictions")
    sj_data.total_cases.plot(ax=axes[0], label="Actual")

    iq_data['fitted'] = iq_model.fittedvalues
    iq_data.fitted.plot(ax=axes[1], label="Predictions")
    iq_data.total_cases.plot(ax=axes[1], label="Actual")

    test_sj, test_iq = read_test_data()
    test_sj = pre_process(feature_selection(test_sj, "sj"))
    test_iq = pre_process(feature_selection(test_iq, "iq"))

    sj_predicts = sj_model.predict(test_sj).astype(int)
    iq_predicts = iq_model.predict(test_iq).astype(int)

    save_results(sj_predicts, iq_predicts, "base_model_5")

    plt.show()

if __name__ == "__main__":
    run_base_model()

    # sj_data, iq_data = read_train_data()

    # sj_data_lagged = sj_data.drop(["total_cases"], axis=1).shift(12)
    # sj_data_lagged.dropna(inplace=True)
    # sj_data_lagged.columns = [str(col) + '_lagged' for col in sj_data_lagged.columns]
    # sj_data = pd.concat([sj_data, sj_data_lagged], axis=1)

    # iq_data_lagged = iq_data.drop(["total_cases"], axis=1).shift(3)
    # iq_data_lagged.dropna(inplace=True)
    # iq_data_lagged.columns = [str(col) + '_lagged' for col in iq_data_lagged.columns]
    # iq_data = pd.concat([iq_data, iq_data_lagged], axis=1)

    # print sj_data

    # visualize_corelations(sj_data)
    # visualize_corelations(iq_data)