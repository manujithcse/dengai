import base_model as bm
from keras.models import Sequential
from keras.layers import Dense
from keras.wrappers.scikit_learn import KerasRegressor

def regressor_model():
    model = Sequential()
    model.add(Dense(100, input_dim=4, kernel_initializer='random_uniform', 
                         bias_initializer='random_uniform', 
                         activation='linear'))
    model.add(Dense(10, kernel_initializer='random_uniform', 
                         bias_initializer='random_uniform', 
                         activation='linear'))
    # model.add(Dense(10, kernel_initializer='uniform', activation='relu'))
    # model.add(Dense(5, kernel_initializer='normal', activation='relu'))
    model.add(Dense(1, kernel_initializer='random_uniform'))
    model.compile(loss='mae', optimizer='adam')
    return model

sj_data, iq_data = bm.read_train_data()

sj_data = bm.feature_selection(sj_data, "sj")
iq_data = bm.feature_selection(iq_data, "iq")

sj_data = bm.pre_process(sj_data)
iq_data = bm.pre_process(iq_data)

sj_data_features = sj_data.drop(["total_cases"], axis=1).values
sj_data_labels = sj_data[["total_cases"]].values

# print sj_data_labels

estimator = KerasRegressor(build_fn=regressor_model, 
                           epochs=10000, 
                           batch_size=50, 
                           verbose=1,
                           validation_split=0.2)

sj_results = estimator.fit(sj_data_features, sj_data_labels)
sj_data["fitted"] = estimator.predict(sj_data_features)
bm.plot_fitted(sj_data)


# print results.history

# y_1 = estimator.predict(X_1.values)
# y_1 = estimator.predict(X_test.values)