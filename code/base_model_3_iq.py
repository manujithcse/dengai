import pandas as pd
import numpy as np

from matplotlib import pyplot as plt
import seaborn as sns

import statsmodels.api as sm
from statsmodels.tools import eval_measures
import statsmodels.formula.api as smf

from sklearn.tree import DecisionTreeRegressor, ExtraTreeRegressor
from sklearn.ensemble import RandomForestRegressor, AdaBoostRegressor, GradientBoostingRegressor, ExtraTreesRegressor, BaggingRegressor
import sklearn
from sklearn.pipeline import Pipeline

from sklearn.decomposition import PCA
from sklearn.feature_selection import SelectKBest
from sklearn.grid_search import GridSearchCV
from sklearn.linear_model import SGDRegressor, Ridge, ElasticNet

from sklearn.svm import SVR

def get_nb_model_formula(features):
        model_formula = "total_cases ~ 1"
        for k in features:
                model_formula = model_formula + " + " + k
        return model_formula


def get_lagged_nb_model(train, test, features):

    # Step 1: specify the form of the model
    model_formula = get_nb_model_formula(features)

    grid = 10 ** np.arange(-8, -3, dtype=np.float64)

    best_alpha = []
    best_score = 1000

    # Step 2: Find the best hyper parameter, alpha
    for alpha in grid:
        model = smf.glm(formula=model_formula,
                        data=train,
                        family=sm.families.NegativeBinomial(alpha=alpha))

        results = model.fit()
        predictions = results.predict(test).astype(int)
        score = eval_measures.meanabs(predictions, test.total_cases)

        if score < best_score:
            best_alpha = alpha
            best_score = score

    print('best alpha = ', best_alpha)
    print('best score = ', best_score)

    # Step 3: refit on entire dataset
    full_dataset = pd.concat([train, test])
    model = smf.glm(formula=model_formula,
                    data=full_dataset,
                    family=sm.families.NegativeBinomial(alpha=best_alpha))

    fitted_model = model.fit()
    return fitted_model

all_data = pd.read_csv("../dataset/iq/selected.csv", index_col=[0, 1, 2])

# all_data = all_data[['reanalysis_specific_humidity_g_per_kg', 
#                  'reanalysis_dew_point_temp_k', 
#                  'reanalysis_min_air_temp_k', 
#                  'total_cases']]

all_data.fillna(method="ffill", inplace=True)

data = all_data[:520]
predict = all_data[520:]

split = int(data.shape[0]*0.85)
# print split
train = data[:split]
test = data[split:]

train_y = train["total_cases"]
train_x = train.drop("total_cases", axis=1)

test_y = test["total_cases"]
test_x = test.drop("total_cases", axis=1)

columns = list(data.columns) 
columns.remove("total_cases")

# scaler = sklearn.preprocessing.StandardScaler()
# train_x = scaler.fit_transform(train_x)
# test_x = scaler.transform(test_x)
# model = get_lagged_nb_model(train, test, columns)

# print train_x.head(10)

seed = 7

features = []
# features.append(('pca', PCA(n_components=10)))
# features.append(('select_best', SelectKBest(k=10)))
# feature_union = FeatureUnion(features)

# pca = PCA(n_components=20)
# features = pca.fit_transform(train_x)
# print features

estimators = []
# estimators.append(('feature_union', PCA(n_components=20)))
# estimators.append(('feature_union', SelectKBest(k=20)))
estimators.append(('standardize', sklearn.preprocessing.StandardScaler()))
# estimators.append(('regressor', RandomForestRegressor(n_estimators=300, 
#                                                       max_depth=5,
#                                                       max_features=20,
#                                                       oob_score=True,
#                                                       criterion="mae", 
#                                                       random_state=seed)))

estimators.append(("regressor", AdaBoostRegressor(DecisionTreeRegressor(
                                                    max_depth=250,
                                                    max_features=1),
                                                    n_estimators=500,
                                                    loss="exponential", 
                                                    random_state=seed)))

# estimators.append(("regressor", AdaBoostRegressor(DecisionTreeRegressor(
#                                                     max_depth=10,
#                                                     max_features=3),
#                                                     n_estimators=1500,
#                                                     loss="exponential", 
#                                                     random_state=seed)))

# estimators.append(("regressor", GradientBoostingRegressor(
#                                                     max_depth=20,
#                                                     max_features=20,
#                                                     n_estimators=500,
#                                                     loss="lad",
#                                                     random_state=seed)))

# estimators.append(("regressor", AdaBoostRegressor(ExtraTreeRegressor(
#                                                     max_depth=20,
#                                                     max_features=5),
#                                                     n_estimators=500,
#                                                     loss="exponential", 
#                                                     random_state=seed)))

# estimators.append(("regressor", BaggingRegressor(DecisionTreeRegressor(
#                                                     max_depth=10,
#                                                     max_features=5),
#                                                     n_estimators=500,
#                                                     random_state=seed)))

# estimators.append(("regressor", SVR(kernel="rbf")))

# estimators.append(("regressor", ElasticNet()))

model = Pipeline(estimators)
model.fit(train_x, train_y)

test_predict = model.predict(test_x).astype(int)
# # print test_predict
# # print test_y
sns.distplot(test_predict)
plt.show()
score = eval_measures.meanabs(test_predict, test_y)
print score

predicts = model.predict(predict.drop("total_cases", axis=1)).astype(int)
np.savetxt("basemodel3_iq.csv", predicts, delimiter="\n", fmt="%d")


# scaler = sklearn.preprocessing.StandardScaler()
# train_x = scaler.fit_transform(train_x)
# test_x = scaler.transform(test_x)

# model = AdaBoostRegressor(DecisionTreeRegressor(
#                                                 max_depth=20,
#                                                 max_features=5),
#                                                 n_estimators=500,
#                                                 loss="exponential", 
#                                                 random_state=seed)
# model.fit(train_x, train_y)
# test_predict = model.predict(test_x)
# print eval_measures.meanabs(test_predict, test_y)


# param_grid = {
#         "n_estimators": [500],
#         "base_estimator__max_depth": [i for i in range(1, 50)],
# }

# model = AdaBoostRegressor(DecisionTreeRegressor(), 
#                         loss="exponential", random_state=seed)

# bmodel = GridSearchCV(model, param_grid, verbose=1)
# bmodel.fit(train_x, train_y)

# print bmodel.best_params_

# test_predict = bmodel.predict(test_x)
# print eval_measures.meanabs(test_predict, test_y)
# sns.distplot(test_predict)
# plt.show()

