import pandas as pd
import matplotlib.pyplot as plt
from sklearn.preprocessing import StandardScaler

def plot_corr(df,size=10):
    '''Function plots a graphical correlation matrix for each pair of columns in the dataframe.

    Input:
        df: pandas DataFrame
        size: vertical and horizontal size of the plot'''

    corr = df.corr()
    fig, ax = plt.subplots(figsize=(size, size))
    ax.matshow(corr)
    ax.legend(loc='best')
    plt.xticks(range(len(corr.columns)), corr.columns)
    plt.yticks(range(len(corr.columns)), corr.columns)
    plt.xticks(rotation=90)

def correlation_matrix(df):
    from matplotlib import pyplot as plt
    from matplotlib import cm as cm

    fig = plt.figure()
    ax1 = fig.add_subplot(111)
    cmap = cm.get_cmap('jet', 30)
    corr = df.corr()
    cax = ax1.imshow(corr, interpolation="nearest", cmap=cmap)
    ax1.grid(True)
    plt.title('Abalone Feature Correlation')
    labels=corr.columns
    plt.xticks(range(len(corr.columns)), corr.columns)
    plt.yticks(range(len(corr.columns)), corr.columns)
    plt.xticks(rotation=90)
    # ax1.set_xticklabels(range(len(labels)), labels,fontsize=6)
    # ax1.set_yticklabels(labels,fontsize=6)
    # Add colorbar, make sure to specify tick locations to match desired ticklabels
    fig.colorbar(cax, ticks=[.75,.8,.85,.90,.95,1])
    plt.show()

def draw_totals(dataframe):
    dataframe = dataframe[["ndvi_ne", "total_cases"]]

    scaler = StandardScaler()
    dataframe[["total_cases"]] = scaler.fit_transform(dataframe[["total_cases"]])

    dataframe.plot.line()
    plt.show()


dataframe = pd.read_csv("../dataset/sj/combined.csv")
draw_totals(dataframe)
# correlation_matrix(dataframe)



