import utils
import preprocessor
import feature_extractor
from utils import save_results
from lagged_correlation_model import get_lagged_nb_model

from matplotlib import pyplot as plt

sj_data, iq_data = utils.read_train_data()

sj_all_lagged, sj_lags = feature_extractor.get_lagged_feature_df(sj_all, "sj")
iq_all_lagged, iq_lags = feature_extractor.get_lagged_feature_df(iq_all, "iq")

