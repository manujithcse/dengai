import utils
import preprocessor
import feature_extractor
from utils import save_results
from lagged_correlation_model import get_lagged_nb_model

from matplotlib import pyplot as plt

sj_data, iq_data = utils.read_train_data()
sj_all, iq_all = preprocessor.get_test_train_integrated_data()

sj_all_lagged, sj_lags = feature_extractor.get_lagged_feature_df(sj_all, "sj")
iq_all_lagged, iq_lags = feature_extractor.get_lagged_feature_df(iq_all, "iq")

sj_all_lagged["weekofyear"] = sj_all["weekofyear"]

sj_all = sj_all_lagged[["weekofyear", "station_avg_temp_c", "reanalysis_specific_humidity_g_per_kg", "total_cases"]]

sj_all = sj_all[11:]
sj_all = preprocessor.fill_missing_values(sj_all, "ffill")
# sj_all = preprocessor.scale_features(sj_all)

sj_data = sj_all.head(936-11)
sj_predict = sj_all.tail(sj_all.shape[0] - (936-11))
# sj_data = sj_data[11:]

# sj_data.dropna(inplace=True)
# sj_data = preprocessor.fill_missing_values(sj_data, "ffill")
# sj_predict = preprocessor.fill_missing_values(sj_predict, "ffill")



sj_data_train, sj_data_test = utils.split_train_test_in_order(sj_data)

sj_lags = {
    "station_avg_temp_c":"", "reanalysis_specific_humidity_g_per_kg": "", "weekofyear":""
}
sj_model = get_lagged_nb_model(sj_data_train, sj_data_test, sj_lags)

# sj_data = sj_data.reset_index(drop=False)
figs, axes = plt.subplots(nrows=2, ncols=1)
sj_data['fitted'] = sj_model.fittedvalues
sj_data.fitted.plot(ax=axes[0], label="Predictions")
sj_data.total_cases.plot(ax=axes[0], label="Actual")


predicts = sj_model.predict(sj_predict).astype(int)
predicts.to_csv("basemodel2.csv")

plt.show()

